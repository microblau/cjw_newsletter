<?php
/**
 * File containing the CjwNewsletterUtils class
 *
 * @copyright Copyright (C) 2007-2012 CJW Network - Coolscreen.de, JAC Systeme GmbH, Webmanufaktur. All rights reserved.
 * @license http://ez.no/licenses/gnu_gpl GNU GPL v2
 * @version //autogentag//
 * @package cjw_newsletter
 * @filesource
 */
/**
 * class with some useful functions
 *
 * @version //autogentag//
 * @package cjw_newsletter
 */
class CjwNewsletterUtils extends eZPersistentObject
{

    function __construct(){ }

    /**
     * generate a unique hash md5
     *
     * @param string $flexibleVar is used as a part of string for md5
     * @return string md5
     */
    static function generateUniqueMd5Hash( $flexibleVar = '' )
    {
        $stringForHash = $flexibleVar. '-'. microtime( true ). '-' . mt_rand(). '-' . mt_rand();
        return md5( $stringForHash );
    }

    /**
     * Send unsubscribe notification
     *
     * @param $newsletterSubscription
     * @return null|bool
     */
    static function unsubscribeNotification( $newsletterSubscription )
    {
        if (!($newsletterSubscription instanceof CjwNewsletterSubscription))
        {
            return;
        }
        $cjwNewsletterIni = eZINI::instance( 'cjw_newsletter.ini' );
        $isNotificationEnabled = ($cjwNewsletterIni->variable( 'NewsletterSettings', 'SendUnsubscribeNotification' ) === 'true');
        $isSubscriptionStatusOK = ($newsletterSubscription->attribute('status') == CjwNewsletterSubscription::STATUS_REMOVED_SELF);
        if (!$isNotificationEnabled || !$isSubscriptionStatusOK)
        {
//            return null;
        }
        $notificationEmail = $cjwNewsletterIni->variable( 'NewsletterSettings', 'UnsubscribeNotificationEmail' );
        return self::sendNotificationMail( $newsletterSubscription ,
                                        $notificationEmail,
                                        'design:newsletter/mail/notification_unsubscribe.tpl' );
    }

    /**
     * Generic notification mail sending
     *
     * @param $newsletterSubscription
     * @param $notificationEmailReceiver
     * @param $mailTemplate
     * @return bool
     */
    static protected function sendNotificationMail( $newsletterSubscription, $notificationEmailReceiver = '', $mailTemplate = 'design:newsletter/mail/notification_unsubscribe.tpl' )
    {
        $ini = eZINI::instance( 'site.ini' );
        $cjwNewsletterIni = eZINI::instance( 'cjw_newsletter.ini' );
        $hostName = eZSys::hostname();

        $newsletterUser = $newsletterSubscription->attribute('newsletter_user');

        $tpl = eZTemplate::factory();
        $tpl->setVariable( 'newsletter_subscription', $newsletterSubscription );
        $tpl->setVariable( 'newsletter_user', $newsletterUser );
        $tpl->setVariable( 'hostname', $hostName );
        $templateResult = $tpl->fetch( $mailTemplate ) ;

        // get subject from template var definition
        if ($tpl->hasVariable('subject'))
        {
            $subject = $tpl->variable('subject');
        }

        $emailSender = $cjwNewsletterIni->variable( 'NewsletterMailSettings', 'EmailSender' );
        $emailSenderName = $cjwNewsletterIni->variable( 'NewsletterMailSettings', 'EmailSenderName' );
        $emailReceiver = (empty($notificationEmailReceiver)) ? $ini->variable('MailSettings', 'AdminEmail') : $notificationEmailReceiver;

        $emailReplyTo = $cjwNewsletterIni->variable( 'NewsletterMailSettings', 'EmailReplyTo' );
        $emailReturnPath = $cjwNewsletterIni->variable( 'NewsletterMailSettings', 'EmailReturnPath' );

        $emailReceiverName = '';

        $emailSubject = $subject;
        $emailBody['text'] = $templateResult;

        $cjwMail = new CjwNewsletterMail();
        // x header set for current user
        $cjwMail->setExtraMailHeadersByNewsletterUser( $newsletterUser );
        $cjwMail->setTransportMethodDirectlyFromIni();

        $sendResult = $cjwMail->sendEmail( $emailSender,
                                          $emailSenderName,
                                          $emailReceiver,
                                          $emailReceiverName,
                                          $emailSubject,
                                          $emailBody,
                                          false,
                                          'utf-8',
                                          $emailReplyTo,
                                          $emailReturnPath );
        return $sendResult;
    }
}
