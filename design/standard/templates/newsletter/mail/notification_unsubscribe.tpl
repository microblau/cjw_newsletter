{*?template charset=utf-8?*}{set-block variable=$subject scope=root}{ezini('NewsletterMailSettings', 'EmailSubjectPrefix', 'cjw_newsletter.ini')} {'Notification: unsubscribe'|i18n( 'cjw_newsletter/notification' )}{/set-block}
{*
$newsletter_subscription
$newsletter_user
$hostname
*}
{def $remainingSubscriptionListString = ''}
{foreach $newsletter_user.subscription_array as $subscription}
{if $subscription.id|eq($newsletter_subscription.id)}{skip}{/if}
{set $remainingSubscriptionListString = concat( $remainingSubscriptionListString, "\n- ", $subscription.newsletter_list.main_node.parent.name, " - ", $subscription.newsletter_list.name|wash() )}
{/foreach}
{'Hello,

The user %name with email address %email

has been removed from this newsletter:

%newsletterListName

'|i18n( 'cjw_newsletter/mail/notification',,
                                         hash( '%name', concat( $newsletter_user.first_name, ' ', $newsletter_user.last_name ),
                                               '%email', $newsletter_user.email,
                                               '%newsletterListName', concat($newsletter_subscription.newsletter_list.main_node.parent.name , ' - ', $newsletter_subscription.newsletter_list.name)|wash()
                                                ) )}

{if $remainingSubscriptionListString|trim|ne('')}
{'But this user is still subscribed to this newsletter lists:

%remainingSubscriptionListString

'|i18n( 'cjw_newsletter/mail/notification',,
                                         hash( '%remainingSubscriptionListString', $remainingSubscriptionListString
                                                ) )}
{/if}
{include uri="design:newsletter/mail/footer.tpl"}
